from os import environ,unlink
from subprocess import Popen
from tempfile import NamedTemporaryFile
from sys import exit,stderr
from argparse import ArgumentParser
import gzip

def iwrite(message):
	stderr.write(message)
	stderr.flush()

parser = ArgumentParser(description="Turn a filtered reads tsv file from pytadbit into a hic file for Juicebox")
parser.add_argument('-tsv',
					dest="tsv",
					metavar="TSV",
					type=str,
					help="Path to the filtered read file")
parser.add_argument('-mnd',
					dest="mnd",
					metavar="MND",
					type=str,
					help="Path to the mnd file")
parser.add_argument('-smnd',
					dest="smnd",
					metavar="SMND",
					type=str,
					help="Path to the sorted mnd")
parser.add_argument('-fai',
					dest="fai",
					metavar="FAI",
					type=str,
					help="Path to fai file",
					required=True)
parser.add_argument('-prefix',
					dest="prefix",
					metavar="PREFIX",
					type=str,
					help="Prefix for output files")

parser.add_argument('-d',
					dest="diag",
					action="store_true",
					help="Only diagonal hicfile")

args = parser.parse_args()

tsv = args.tsv
mnd = args.mnd
sorted_mnd = args.smnd
fai = args.fai
prefix = args.prefix
diag = args.diag

if (not sorted_mnd) and (not mnd):
	if tsv:
		mnd = prefix + "_mnd.txt"
		iwrite("Writing {} file\n".format(mnd))
		with gzip.open(tsv,"rt") as ifile, open(mnd,"w") as ofile:
			row_format = "{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}\n".format
			for line in ifile:
				if not (line.startswith("#")):
					line = line.rstrip()
					sp = line.split("\t")
					row = [None]*16
					row[0] = sp[3] ## Strand read 1
					row[1] = sp[1] ## Chromosome read 1
					row[2] = sp[2] ## Position read 1
					row[3] = str(int(sp[5]) + (int(sp[6]) -int(sp[5]))//2) ## Fragment 1 center
					row[4] = sp[9] ## Strand read 2
					row[5] = sp[7] ## Chromosome read 2
					row[6] = sp[8] ## Position read 2
					row[7] = str(int(sp[11]) + (int(sp[12]) -int(sp[11]))//2)
					row[8] = str(255) ## Dummy MAPQ1
					row[9] = sp[4] + "M" ##Dummy cigar 1 
					row[10] = "A" * int(sp[4]) ## Dummy sequence 1
					row[11] = str(255) ## Dummy MAPQ2
					row[12] = sp[10] + "M" ##Dummy cigar 2
					row[13] = "A" * int(sp[10]) ## Dummy sequence 2
					## Dummy readnames... Risky
					row[14] = sp[0] 
					row[15] = sp[0]
					ofile.write(row_format(*row))
	else:
		exit("Either tsv or mnd file is needed")

if not sorted_mnd:
	iwrite("Fixing mnd file\n")
	
	iwrite("\tFixing column ordering with awk\n")
	tmp = NamedTemporaryFile(delete=False)
	iwrite("\t{}\n".format(tmp.name))
	awkcmd = "awk '{{if ($2 > $6){{ print $5, $6, $7, $8, $1, $2, $3, $4, $12, $13, $14, $9, $10, $11, $15, $16}} else {{print}}}}' {}".format(mnd)
	awk = Popen(awkcmd,stdout=tmp,shell=True)
	awk.wait()
	tmp.close()
	
	iwrite("\tSorting mnd file\n")
	sorted_mnd = prefix + "_sorted_mnd.txt"
	ofile = open(sorted_mnd,"w")
	sortcmd = ["sort","-k2,2d","-k6,6d",tmp.name]
	sort = Popen(sortcmd,stdout=ofile)
	sort.wait()
	unlink(tmp.name)
	ofile.close()

iwrite("Calculating hic file\n")

try:
	JUICER = environ["JUICER"]
except KeyError:
	exit("set the $JUICER environmental variable with the path to a valid Juicer Tools jarfile")

hicfile = prefix + ".hic"

juicercmd = ["java","-jar",JUICER,"pre",sorted_mnd,hicfile,fai]

if diag:
	juicercmd+=["-d"]
juicerlog = open("juicer_pre.log","w")
pre = Popen(juicercmd,stderr=juicerlog,stdout=juicerlog)
pre.wait()
juicerlog.close()

iwrite("Done\n")






