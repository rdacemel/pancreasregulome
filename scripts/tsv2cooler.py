from sys import exit,stderr
from argparse import ArgumentParser
from subprocess import Popen,PIPE
import gzip
import re


def convert_strand(strand):
	if strand==0:
		return("+")
	else:
		return("-")

parser = ArgumentParser(description="Create cooler file from hichips tsv")
parser.add_argument('-tsv',
					dest="tsv",
					metavar="TSV FILE",
					type=str,
					help="tsv file with the filtered contacts to convert to cooler",
					required=False)
parser.add_argument('-fai',
					dest="fai",
					metavar="FAIFILE",
					type=str,
					help="path to faifile to create cooler",
					required=True)
parser.add_argument('-prefix',
					dest="prefix",
					metavar="FILE PREFIXES",
					type=str,
					help="prefix to name intermediates and final files",
					default="nodups",
					required=False)
parser.add_argument('-res',
					dest="res",
					metavar="MINUMUM RESOLUTION",
					type=str,
					help="Minimum resolution to create matrices",
					default="5000",
					required=False)
parser.add_argument('-ass',
					dest="assembly",
					metavar="ASSEMBLY",
					type=str,
					help="Assembly to use",
					default="DanRer10",
					required=False)
parser.add_argument('-spairs',
					dest="spairs",
					metavar="SORTED PAIRS FILE",
					type=str,
					help="Already computed sorted pairs file",
					required=False)


args = parser.parse_args()
tsvfile = args.tsv
prefix = args.prefix
res = args.res
assembly = args.assembly
fai = args.fai
sorted_pairsfile = args.spairs

if not (sorted_pairsfile):
	if not (tsvfile):
		exit("tsv file needed, try again")
	pairsfile = prefix + ".pairs"
	with gzip.open(tsvfile,'rt') as tfile, open(pairsfile,'w') as ofile:
		for line in tfile:
			if (re.search("chrM",line) or re.search("chrUn",line)) :continue
			line = line.rstrip()
			if line.startswith("#"):
				line = re.sub(" CRM","chromsize:",line)
				line = re.sub("\t"," ",line)
				ofile.write(line)
				ofile.write("\n")
			else:
				spline = line.split("\t")
				rname,chrom1,pos1,strand1,_,_,_,chrom2,pos2,strand2,_,_,_ = spline
				strand1,strand2 = convert_strand(strand1),convert_strand(strand2)
				row = "\t".join([rname,chrom1,pos1,chrom2,pos2,strand1,strand2,"LL\n"])
				ofile.write(row)

	sorted_pairsfile = prefix + ".sorted.pairs.gz"
	sfile = open(sorted_pairsfile,"wb")
	stderr.write("Sorting pairs file\n")
	command1 = ["sort","-k2,2","-k4,4","-k3,3n","-k5,5n",pairsfile]
	process1 = Popen(command1,stdout=PIPE)
	command2 = ["bgzip","-c"]
	process2 = Popen(command2,stdin=process1.stdout,stdout=sfile)
	process2.communicate()
	sfile.close()

	stderr.write("Indexing pairs file\n")
	command3 = ["pairix","-f",sorted_pairsfile]
	process3 = Popen(command3)
	process3.wait()


stderr.write("Creating cooler file")
cooler_file = prefix + "." + res + ".cool"
fai_res = fai + ":" + res

command4 = ["cooler","cload","pairix","--assembly",assembly,fai_res,sorted_pairsfile,cooler_file]
process4 = Popen(command4)
process4.wait()

stderr.write("Aggregating cooler file")

command5 = ["cooler","zoomify",cooler_file]
process5 = Popen(command5)
process5.wait()










#sort -k2,2 -k4,4 -k3,3n -k5,5n nodups.tsv |bgzip -c > nodups.pairs.gz
