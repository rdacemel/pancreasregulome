#!/home/ska/rafa/programs/miniconda2/bin/python

from argparse import ArgumentParser
import gem
from os.path import basename,splitext
from os import makedirs,mkdir,listdir,unlink,errno
from pytadbit.mapping.full_mapper import full_mapping
from pytadbit.parsers.genome_parser import parse_fasta
from pytadbit.parsers.map_parser import parse_map
from pytadbit.mapping import get_intersection
from pytadbit.mapping.filter import filter_reads,apply_filter
from pytadbit.mapping.analyze import insert_sizes,hic_map
from multiprocessing import Pool
from subprocess import Popen
import re
from sys import stderr

def mapping(arg):
	fastq,out_dir,temp_dir = arg
	mapped = full_mapping(gem_index_path=gem_index,
					   out_map_dir=out_dir,
					   fastq_path=fastq,
					   r_enz=renz, 
					   frag_map=True, 
					   clean=True, 
					   nthreads=cpus//2,
					   temp_dir=temp_dir)
	return(mapped)

parser = ArgumentParser(description="Pipeline to process HiChIPs from fastqs to matrices")
parser.add_argument('-fq1',
					dest="fastq1",
					metavar="FASTQ1",
					type=str,
					help="Fastq file with path to read1",
					required=False)
parser.add_argument('-fq2',
					dest="fastq2",
					metavar="FASTQ2",
					type=str,
					help="Fastq file with path to read2",
					required=False)
parser.add_argument('-i,--gem_index',
					dest="gem_index",
					metavar="GEMTOOLS_INDEX",
					type=str,
					help="Gemtools index file",
					required=False)
parser.add_argument('-fasta',
					dest="fasta",
					metavar="FASTA_FILE",
					type=str,
					help="Fasta file with reference genome",
					required=False)
parser.add_argument('-n,--name',
					dest="exp_name",
					metavar="EXP_NAME",
					type=str,
					help="Experiment name to name output files",
					default="Untitled_HiChIP",
					required=False)
parser.add_argument('-r,--res',
					dest="resolution",
					metavar="MATRIX_RESOLUTION",
					type=int,
					help="Final resolution of output matrices",
					required=False)
parser.add_argument('-p,--cpus',
					dest="cpus",
					metavar="CPUS",
					type=int,
					help="Number of cpus to use",
					default=1,
					required=False)
parser.add_argument('-e',
					dest="renz",
					metavar="RESTRICTION_ENZYME",
					type=str,
					help="Restriction enzyme used in the experiment",
					default="dpnII",
					required=False)
parser.add_argument('-map1',
					dest="mapfolder1",
					metavar="MAP_FOLDER1",
					type=str,
					help="Folder with already mapped mate 1 reads",
					required=False)
parser.add_argument('-map2',
					dest="mapfolder2",
					metavar="MAP_FOLDER2",
					type=str,
					help="Folder with already mapped mate 2 reads",
					required=False)
parser.add_argument('-tsv',
					dest="filtered_reads",
					metavar="TSV_FOLDER",
					type=str,
					help="Tsv file with already filtered reads",
					required=False)
parser.add_argument('-focus',
					dest="focus",
					metavar="FOCUS",
					type=str,
					help="Extract matrix from given scaffold/chromosome",
					required=False)

args = parser.parse_args()
global gem_index
global cpus
cpus = args.cpus
global renz
renz = args.renz

if not ((args.fastq1 and args.fastq2 and args.fasta) or args.filtered_reads or (args.mapfolder1 and args.mapfolder2 and args.fasta)):
	parser.error("Missing arguments, exiting")

if args.filtered_reads:
	filtered_reads = args.filtered_reads
elif (args.mapfolder1 and args.mapfolder2):
	mapped1 = [args.mapfolder1 + x for x in listdir(args.mapfolder1)]
	mapped2 = [args.mapfolder2 + x for x in listdir(args.mapfolder2)]
	stderr.write(mapped1[0])
	stderr.write(mapped2[0])
	stderr.flush()
	
else:
	if not (args.gem_index):
		print "[Rafa said]Generating gemtools index..."
		gem_index = gem.index(args.fasta,"index",threads=cpus)
	else:
		gem_index = args.gem_index
	print "[Rafa said]Preparing fastqs and mapping..."
	pool = Pool(3)
	loa = [(args.fastq1,"map1","tmp1"),(args.fastq2,"map2","tmp2")]
	mapfiles_list = []
	for mapped in pool.imap(mapping,loa,chunksize=1):
		mapfiles_list.append(mapped)
	pool.close()
	pool.join()
	mapped1,mapped2 = mapfiles_list

if not args.filtered_reads:
	print "[Rafa said]Parsing genome..."
	genome_seq = parse_fasta(args.fasta)
	print "[Rafa said]Parsing reads..."
	try:
		mkdir("tsv")
	except OSError as error:
		if error.errno == errno.EEXIST:
			pass
		else:
			raise
	tsv1 = "tsv/" + args.exp_name + "_1.tsv"
	tsv2 = "tsv/" + args.exp_name + "_2.tsv"
	parse_map(mapped1,mapped2,tsv1,tsv2,genome_seq=genome_seq,re_name=renz,clean=True,verbose=True)
	reads = "tsv/"+ args.exp_name + "_raw.tsv"
	print "[Rafa said]Intersecting reads..."
	get_intersection(tsv1,tsv2,reads)
	max_size = int(insert_sizes(reads,show=True,nreads=1000000,savefig="insert_sizes.png")[1])
	print "[Rafa said]Max insert size from distribution is {}".format(max_size)
	print "[Rafa said]Filtering reads..." 
	masked = filter_reads(reads,
	        max_molecule_length=max_size,
	        over_represented=0.005,
	        max_frag_size=100000,
	        min_frag_size=100,
	        re_proximity=5,
	        min_dist_to_re=1000)
	filtered_reads = "tsv/" + args.exp_name + "_filtered_reads.tsv"
	apply_filter(reads, filtered_reads, masked,filters=[1, 2, 3, 4, 6, 7, 9, 10])

if args.resolution:
	print "[Rafa said]Extracting matrices of {} bp resolution ...".format(args.resolution)

	try:
		folder = "mat/{}kb/".format(str(args.resolution//1000))
		makedirs(folder)
	except OSError as error:
		if error.errno == errno.EEXIST:
			pass
		else:
			raise

	if args.focus:
		matname = folder + args.focus + ".mat"
		hic_map(filtered_reads,
			resolution=args.resolution,
			focus=args.focus,
			savedata=matname)
	else:
		hic_map(filtered_reads,
			resolution=args.resolution,
			by_chrom="intra",
			savedata=folder)

print "[Rafa said]Finish"