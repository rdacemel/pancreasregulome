# Pancreas Regulome HiChIP analysis

Custom scripts used to analyze HiChiP data in the zebrafish pancreas regulome paper (R. Bordeira-Carriço , J. Teixeira et al., see  https://doi.org/10.1101/2020.04.27.064220 ).

## HiChIP analysis from raw fastq files to visualization 

### From fastq-files to filtered pairs and matrix files 

The analysis from the two fastq files received of Illumina Paired-End sequencing to the contact matrices is performed with a Python2 script called [hichips.py](scripts/hichips.py). This code wraps extensively several [TadBit](https://3dgenomes.github.io/TADbit/) functions to map the reads, assign reads to restriction fragments, filter non-informative pairs and calculate contact matrices. It uses [GEM mapper](https://github.com/smarco/gem3-mapper) to perform the alignments and requires the [pytadbit](https://github.com/zilhua/tadbit) and the [multiprocessing](https://docs.python.org/2/library/multiprocessing.html) packages. The help message is the following: 

```
usage: hichips.py [-h] [-fq1 FASTQ1] [-fq2 FASTQ2]
                  [-i,--gem_index GEMTOOLS_INDEX] [-fasta FASTA_FILE]
                  [-n,--name EXP_NAME] [-r,--res MATRIX_RESOLUTION]
                  [-p,--cpus CPUS] [-e RESTRICTION_ENZYME] [-map1 MAP_FOLDER1]
                  [-map2 MAP_FOLDER2] [-tsv TSV_FOLDER] [-focus FOCUS]

Pipeline to process HiChIPs from fastqs to matrices

optional arguments:
  -h, --help            show this help message and exit
  -fq1 FASTQ1           Fastq file with path to read1
  -fq2 FASTQ2           Fastq file with path to read2
  -i,--gem_index GEMTOOLS_INDEX
                        Gemtools index file
  -fasta FASTA_FILE     Fasta file with reference genome
  -n,--name EXP_NAME    Experiment name to name output files
  -r,--res MATRIX_RESOLUTION
                        Final resolution of output matrices
  -p,--cpus CPUS        Number of cpus to use
  -e RESTRICTION_ENZYME
                        Restriction enzyme used in the experiment
  -map1 MAP_FOLDER1     Folder with already mapped mate 1 reads
  -map2 MAP_FOLDER2     Folder with already mapped mate 2 reads
  -tsv TSV_FOLDER       Tsv file with already filtered reads
  -focus FOCUS          Extract matrix from given scaffold/chromosome
``` 

It works using several files as inputs. 

#### From fastq files 

In order to run the analysis from the raw fastq files, the two first options must be specified (-fq1 and -fq2). They are the path to these files. Additionally, a fasta file with the reference genome is also needed (-fasta). Then, the GEM index to perfom the analysis will be calculated. In order to save time, if a GEM index for our particular reference genome has been already created it can be specified with the -i option. If not specified with -e the restriction enzyme used to generate the restriction map is DpnII. With -n the prefix for the output files can be specified. If a resolution is specified with -r, matrix files of this resolution are also created. If not, only the filtered pairs file is generated. With -p the number of cores for the creation of the GEM index and for the mapping is specified. The minimum number of cores used is two. 

```sh
python scripts/hichips.py -fq1 r1.fq.gz \\ 
-fq2 r2.fq.gz \\
-fasta genome.fa \\
-name prefix \\
-r 10000 \\
-p 10 1>hichip.out 2>hichip.err &
```

Several folders with output files are created. The map folder contains the alignment files in GEM MAP format. These are tsv files and several fields are considered for further processing. The first field, for instance, is the name of the read. This allows to find the read pair in the other fastq file. The second field is the sequence of the read. The third one is the quality of the alignment but it is not used. The fourth is not used either. The fifth field is separated by the colon in four subfields and they are the chromosome, the strand, the genomic coordinate and the length of the alignment. Then, the reads are parsed to a raw tsv file with 6 fields: read name, chromosome, genomic coordinate, strand (1 for positive and 0 for negative), alignment length, closest restriction site upstream and closest restriction site downstream. This file also contains a header with the size of each of the chromosomes of the reference used. Then, read pairs are intersected and the result is stored in a 12 column raw read pairs tsv file. After that, reads are filtered out due to different criteria. Self circles, dangling ends and errors are all cases where both reads of the pair come from the same restriction fragment and therefore are not informative. The classification of these categories depend upon the relative orientation of the reads. Extra dangling ends pairs belong to different restriction fragments but they are close enough so they might be the result of an incomplete digestion, and they are filtered out as well. The pairs in the category too close from REs are those in which the start of one of the reads is only 5 bp away from a restriction site. In addition, reads that are too far from the closest restriction site are also filtered out. The cutoff is established according to the insert size of the library. The too short category filters those pairs involving restriction fragments smaller than 100bp. The too large category filters those pairs involving very large restriction fragments, bigger than 100kb, that are often repetitive regions. The over-represented category would filter out reads in restriction fragments that are overly enriched in the library, but in the case of the HiChIP this is expected so this reads are kept. Finally, duplicated pairs are also filtered out. A file for each filtered category is created, containing the name of the reads included in that classification. Then, also a tsv file containing the information of the valid read pairs that is later used for the calculation of the contact matrices. The matrix files are simply tsv files with the numbers from the square dense contact matrix at a given resolution.

#### From alignment files

The program can also be run directly from aligned pairs using the -map1 and -map2 options. 

```sh
cd results
python hichips.py -map1 map1/ \\ 
-map2 map2/ \\
-fasta genome.fa \\
-name prefix \\
-r 10000 1>hichip.out 2>hichip.err &
```

#### From filtered read files

Finally, matrices can also be calculated directly from tsv files at different resolutions with the -tsv option. The -focus option can be used in order to choose a specific chromosome. 

```sh
cd results
python hichips.py -tsv tsv/prefix_filtered_reads.tsv \\
-focus chr1 \\
-r 20000 1>hichip.out 2>hichip.err
```

### From filtered pairs to higlass visualization

In order to visualize the HiChIP experiments in higlass we need to generate cooler files, that are binarily compressed representations of multiresolution matrices. This is automatized by the the Python3 script [tsv2cooler.py](scripts/tsv2cooler.py) that uses the [cooler](https://github.com/mirnylab/cooler) library to produce those matrices from the filtered reads tsv file. This file needs to be tweaked a bit and then the pairs need to be sorted (UNIX sort), compressed using [bgzip](https://github.com/samtools/htslib) and indexed with [pairix](https://github.com/4dn-dcic/pairix) before being passed to [cooler](https://github.com/mirnylab/cooler). Here is the help message of the program, that also take different input files: 

```
usage: tsv2cooler.py [-h] [-tsv TSV FILE] -fai FAIFILE [-prefix FILE PREFIXES]
                     [-res MINUMUM RESOLUTION] [-ass ASSEMBLY]
                     [-spairs SORTED PAIRS FILE]

Create cooler file from hichips tsv

optional arguments:
  -h, --help            show this help message and exit
  -tsv TSV FILE         tsv file with the filtered contacts to convert to
                        cooler
  -fai FAIFILE          path to faifile to create cooler
  -prefix FILE PREFIXES
                        prefix to name intermediates and final files
  -res MINUMUM RESOLUTION
                        Minimum resolution to create matrices
  -ass ASSEMBLY         Assembly to use
  -spairs SORTED PAIRS FILE
                        Already computed sorted pairs file
``` 

The most common way of using the program is using the [tsv file](tsv/prefix_filtered_reads.tsv.gz) with the filtered reads in the -tsv flag: 

```sh
python scripts/tsv2cooler.py -tsv tsv/prefix_filtered_reads.tsv.gz \\
-fai genome.fa.fai \\
-prefix prefix \\
-res 5000 1>cool.out 2>cool.err &
```

This generate first a spairs file, a file with the pairs that is sorted, compressed and indexed. This is passed to cooler in order to calculate first a 5kb resolution cooler file and then the multiresolution cooler file with a maximum resolution of 5kb and a minimum of 2Mb that can be loaded in a local instance of [higlass](http://higlass.io/). This local instance was generated using a docker container in a local server, following this [instructions](https://github.com/higlass/higlass-docker). If not stated otherwise, HiChIP snapshots shown in the figures are obtained from the higlass browser with the standard visualization (that uses a log2 transformation of the interaction frequency). It is important to note that the ordering of the matrix will follow the order of the chromosomes in the fai file.  

Alternatively, the intermediate sorted pairs file can be used if something went wrong while calling cooler, skipping the first part: 

```sh
python scripts/tsv2cooler.py -spairs prefix.sorted.pairs.gz \\
-fai genome.fa.fai \\
-prefix prefix \\
-res 5000 \\ 1>cool.out 2>cool.err &
```

### From filtered pairs to juicebox visualization

In addition, hic files compatible with [juicebox](https://www.aidenlab.org/juicebox) visualization were also created. In order to create the hic files we generated a Python3 script called [tsv2hic.py](scripts/tsv2hic.py) that automatized the job. It starts from the tsvfile, converts this file to a slightly different tsv file, sorts it and uses [Juicer Tools pre](https://github.com/aidenlab/juicer/wiki/Juicer-Tools-Quick-Start) to generate the hic file. An environmental variable called JUICER must be set containing the exact path to the jarfile with Juicer Tools. This is the help message from the program: 

```
usage: tsv2hic.py [-h] [-tsv TSV] [-mnd MND] [-smnd SMND] -fai FAI
                  [-prefix PREFIX] [-d]

Turn a filtered reads tsv file from pytadbit into a hic file for Juicebox

optional arguments:
  -h, --help      show this help message and exit
  -tsv TSV        Path to the filtered read file
  -mnd MND        Path to the mnd file
  -smnd SMND      Path to the sorted mnd
  -fai FAI        Path to fai file
  -prefix PREFIX  Prefix for output files
  -d              Only diagonal hicfile
```

The usual way to run it is again from the tsv files: 

```sh
export JUICER=path_to_juicer/juicer_tools_0.7.5.jar # First tell the system where juicer-tools are

python scripts/tsv2hic.py -tsv prefix_filtered_reads.tsv.gz \\
-fai genome.fa.fai \\ #needs to be only the first to columns
-prefix prefix \\
-d 1>hic.out 2>hic.err & #-d only takes into account intrachromosomal contacts, much faster
```

The program outputs several intermediate files, an mnd file that is also a tsv file with the read pairs but with the column order expected by [Juicer Tools pre](https://github.com/aidenlab/juicer/wiki/Juicer-Tools-Quick-Start) and the sorted version of this file as well. Finally, also the hic file is produced. Both the sorted and unsorted intermediate files can be used as input with the -smnd and -mnd options respectively. The mnd file must not be gzipped:  

```sh
# From mnd
python scripts/tsv2hic.py -mnd prefix_mnd.txt \\
-fai genome.fa.fai \\ 
-prefix prefix \\
-d 1>hic.out 2>hic.err & 

#From smnd
python scripts/tsv2hic.py -smnd prefix_sorted_mnd.txt.gz \\
-fai genome.fa.fai \\ 
-prefix prefix \\
-d 1>hic.out 2>hic.err & 
```
